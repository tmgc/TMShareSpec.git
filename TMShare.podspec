#
#  Be sure to run `pod spec lint TMSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.name         = "TMShare"
  s.version      = "0.0.84"
  s.summary      = "TM 分享组件"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "renxukui" => "renxukui@360tianma.com" }
  s.platform     = :ios
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/tmgc/TMShareSpec.git", :tag => s.version }
  s.source_files = 'TMFramework/*.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'TMFramework/*.framework'
  s.resources = "TMFramework/TMShare.bundle"
  s.requires_arc = true
  s.framework = 'SystemConfiguration'
  s.dependency "TMSDK"
  s.dependency "Weibo_SDK",'3.2.5.1'
  s.dependency "TMWechatOpenSDK"
  s.dependency "TMComponentKitSDK"

s.xcconfig = {
  'VALID_ARCHS' =>  'armv7 x86_64 arm64',
}
end
