//
//  TMShareSDKQQApiInterface.h
//  TMShare
//
//  Created by rxk on 2019/9/29.
//  Copyright © 2019 Tianma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "TMSDKTypeDefine.h"
#import "TMSDKPlatformItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface TMShareSDKQQApiInterface : NSObject<TencentSessionDelegate, QQApiInterfaceDelegate>
+(instancetype)sharedManager;
+ (void)setupQQWithAppId:(NSString *)appId universalLink:(NSString *)universalLink;
+ (void)cancelAuthorize;

- (void)loginWithAuthorizeStateChnageHandler:(TMShareSDKAuthorizeStateChangedHandler)handler;
- (void)tm_getAuthorizeCodeWithHandler:(TMShareSDKAuthorizeStateChangedHandler)handler;

- (void)shareQQWithPlatformItem:(TMSDKPlatformItem *)item shareModel:(TMShareSDKModel *)shareModel shareImg:(nullable UIImage*)image onStateChanged:(TMShareSDKShareStateChangedHandler)stateChangedHandler;

@end



//TMShareSDKAuthorizeStateChangedHandler
NS_ASSUME_NONNULL_END
