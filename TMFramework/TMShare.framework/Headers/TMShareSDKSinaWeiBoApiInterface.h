//
//  TMShareSDKSinaWeiBoApiInterface.h
//  TMShare
//
//  Created by rxk on 2019/10/11.
//  Copyright © 2019 Tianma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMSDKTypeDefine.h"
#import "TMSDKPlatformItem.h"
#import "WeiboSDK.h"
NS_ASSUME_NONNULL_BEGIN

@interface TMShareSDKSinaWeiBoApiInterface : NSObject<WeiboSDKDelegate>
+(instancetype)sharedManager;
+ (void)setupSinaWeiBoWithAppkey:(NSString *)appkey redirectURI:(NSString *)redirectURI;
+ (void)cancelAuthorize;

- (void)loginWithAuthorizeStateChnageHandler:(TMShareSDKAuthorizeStateChangedHandler)handler;
- (void)tm_getAuthorizeCodeWithHandler:(TMShareSDKAuthorizeStateChangedHandler)handler;


- (void)shareSinaWithPlatformItem:(TMSDKPlatformItem *)item shareModel:(TMShareSDKModel *)shareModel shareImg:(nullable UIImage*)image onStateChanged:(TMShareSDKShareStateChangedHandler)stateChangedHandler;
@end

NS_ASSUME_NONNULL_END
