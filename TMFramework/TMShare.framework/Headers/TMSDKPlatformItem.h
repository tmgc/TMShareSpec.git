//
//  TMSDKPlatformItem.h
//  TMShare
//
//  Created by rxk on 2019/9/29.
//  Copyright © 2019 Tianma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TMSDKTypeDefine.h"
NS_ASSUME_NONNULL_BEGIN

@interface TMSDKPlatformItem : NSObject

+ (instancetype)itemWithPlatformType:(TMPlatformType)platformType;
@property (assign, nonatomic) TMPlatformType platformType;
@property (copy, nonatomic) NSString *platformId;
@property (strong, nonatomic)  UIImage *iconNormal;
@property (strong, nonatomic)  NSString *platformName;

@end

NS_ASSUME_NONNULL_END
