//
//  TMShareSDK.h
//  TMShare
//
//  Created by rxk on 2019/9/27.
//  Copyright © 2019 Tianma. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TMSDKTypeDefine.h"
#import "TMShareSDKModel.h"



NS_ASSUME_NONNULL_BEGIN

@interface TMShareSDK : NSObject

+ (instancetype)instance;

///获取分享状态
@property (nonatomic, copy) TMShareSDKShareStateChangedHandler shareStateChangedHandler;

+ (BOOL)isClientInstalled:(TMPlatformType)platform;

+ (void)setupQQWithAppId:(NSString *)appId universalLink:(NSString *)universalLink;
+ (void)setupWeChatWithAppId:(NSString *)appId appSecret:(NSString *)appSecret  universalLink:(NSString *)universalLink;
+ (void)setupSinaWeiBoWithAppkey:(NSString *)appkey redirectURI:(NSString *)redirectURI;

+ (void)authorize:(TMPlatformType)platformType
onStateChanged:(TMShareSDKAuthorizeStateChangedHandler)stateChangedHandler;

+ (void)getAuthorizeCodeWithPlatformType:(TMPlatformType)platformType
                          onStateChanged:(TMShareSDKAuthorizeStateChangedHandler)stateChangedHandler;

+ (id)showShareCustomItems:(NSArray *)items
       shareParams:(TMShareSDKModel *)shareModel
    onStateChanged:(TMShareSDKShareStateChangedHandler)stateChangedHandler;

+ (void)dismissShareController;
+ (void)cancelAuthorize:(TMPlatformType)platformType;

@property (nonatomic, strong) TMShareSDKModel *shareModel;
@end

NS_ASSUME_NONNULL_END
