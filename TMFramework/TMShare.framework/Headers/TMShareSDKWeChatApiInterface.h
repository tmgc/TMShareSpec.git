//
//  TMShareSDKWeChatApiInterface.h
//  TMShare
//
//  Created by rxk on 2019/10/11.
//  Copyright © 2019 Tianma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"
#import "TMSDKTypeDefine.h"
#import "TMSDKPlatformItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface TMShareSDKWeChatApiInterface : NSObject<WXApiDelegate>
+(instancetype)sharedManager;

+ (void)setupWeChatWithAppId:(NSString *)appId appSecret:(NSString *)appSecret universalLink:(NSString *)universalLink;
+ (void)cancelAuthorize;

- (void)loginWithAuthorizeStateChnageHandler:(TMShareSDKAuthorizeStateChangedHandler)handler;
- (void)tm_getAuthorizeCodeWithHandler:(TMShareSDKAuthorizeStateChangedHandler)handler;

- (void)shareWechatWithPlatformItem:(TMSDKPlatformItem *)item shareModel:(TMShareSDKModel *)shareModel shareImg:(nullable UIImage*)image onStateChanged:(TMShareSDKShareStateChangedHandler)stateChangedHandler;

@end


NS_ASSUME_NONNULL_END
